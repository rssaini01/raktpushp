package com.raktpushp;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Pattern;

public interface Constants {
	int ACTIVE_ACCOUNT = 1;
	int OTP_LENGTH = 6;

	public static final Locale LOCALE = new Locale("en", "IN");
	public static final SimpleDateFormat TIME_SDF = new SimpleDateFormat("HH:mm:ss");
	public static final SimpleDateFormat DATE_SDF = new SimpleDateFormat("dd-MM-yyyy");
	public static final SimpleDateFormat DATETIME_SDF = new SimpleDateFormat("dd-Mm-yuyy HH:mm:ss");

	public static final String ALPHA_NUMERIC_STRING = "aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ0123456789";
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	public static final String NUMBERS = "0123456789";

	public enum UserRole {
		ADMIN_USER, NORMAL_USER
	}

	public enum TokenStatus {
		LOGGED_IN, LOGGED_OUT
	}

	public enum BloodGroup {
		A_POSITIVE, A_NEGATIVE, B_POSITIVE, B_NEGATIVE, AB_POSITIVE, AB_NEGATIVE, O_POSITIVE, O_NEGATIVE
	}
	
	public enum BloodComponents {
		PLASMA, PLATELETS, CELLS
	}
	
	public enum LocationStatus {
		CURRENT, PAST
	}

	public enum LocationKeys {
		POSTAL_CODE, STATE, ADDRESS, PLACE_ID, LOCALITY
	}

	public enum RequestStatus {
		APPROVED, DECLINED, PENDING
	}

	public enum ResponseCode {
		SUCCESS(200), ERROR(100);

		private int value;

		private ResponseCode(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	public enum Length {
		PASSWORD_SALT(25), API_TOKEN(30), TOKEN_LENGTH(30), OTP_SESSION_ID(20);

		private int value;

		public int getValue() {
			return value;
		}

		private Length(int value) {
			this.value = value;
		}
	}

	public enum MessageType {
		PROMOTIONAL, TRANSACTIONAL
	}

	public enum Gender {
		MALE, FEMALE, OTHER
	}

}
