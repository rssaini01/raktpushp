package com.raktpushp.service;

import java.util.Date;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.raktpushp.AppConfig;
import com.raktpushp.AppUtils;
import com.raktpushp.Constants;
import com.raktpushp.Constants.Length;
import com.raktpushp.Constants.MessageType;
import com.raktpushp.model.OTPModel;
import com.raktpushp.model.UserDetails;
import com.raktpushp.repo.OTPRepo;
import com.raktpushp.repo.UserDetailsRepo;
import com.raktpushp.utils.MessageSample;
import com.raktpushp.utils.MessageUtility;

@Component
public class OTPServiceImpl implements OTPService {

	@Autowired
	private MessageUtility messageUtility;

	@Autowired
	private OTPRepo otpRepo;

	@Autowired
	private UserDetailsRepo userRepo;

	@Override
	public OTPModel sendOTP(UserDetails userDetails, String phoneNo) {
		String otp = AppUtils.getOTP(AppConfig.getOtpLength());
		String otpSessionId = AppUtils.generateToken(Length.OTP_SESSION_ID);
		Date validUpto = getOtpExpiryTime();
		String otpSMS = getMessageForOTP(otp, validUpto);
		String phoneNo2 = userDetails.getPhoneNo();

		if (!phoneNo.equals(phoneNo2) && phoneNo2 == null) {
			userDetails.setPhoneNo(phoneNo);
			userRepo.save(userDetails);
			messageUtility.sendSMS(otpSMS, MessageType.TRANSACTIONAL, phoneNo);
		} else
			messageUtility.sendSMS(otpSMS, MessageType.TRANSACTIONAL, phoneNo2);

		OTPModel otpModel = new OTPModel();
		otpModel.setOtp(otp);
		otpModel.setOtpSessionId(otpSessionId);
		otpModel.setValidUpto(validUpto);
		otpModel.setUserDetails(userDetails);

		otpRepo.save(otpModel);
		return otpModel;
	}

	private Date getOtpExpiryTime() {
		long addMinute = 15 * 60 * 1000;
		Date expiryDate = new Date(System.currentTimeMillis() + addMinute);
		return expiryDate;
	}

	private String getMessageForOTP(String otp, Date expiryTime) {
		String time = Constants.TIME_SDF.format(expiryTime);
		String sms = MessageSample.OTP_SMS_MOCK.replace("${otp}", otp).replace("${time}", time);
		return sms;
	}

	@Override
	public boolean otpVerify(OTPModel otpModel, UserDetails userDetails) {
		try {
			OTPModel dbModel = otpRepo.getOne(otpModel.getOtpSessionId());
			if (userDetails.equals(dbModel.getUserDetails())) {
				Date currentTime = new Date(System.currentTimeMillis());
				Date expiryTime = dbModel.getValidUpto();
				String userOtp = otpModel.getOtp();
				String dbOtp = dbModel.getOtp();

				if (expiryTime.after(currentTime))
					return dbOtp.equals(userOtp) ? updateDetails(dbModel, userDetails) : false;
				else
					return false;
			} else
				return false;
		} catch (PersistenceException e) {
			System.err.println(e.getMessage());
			e.printStackTrace(System.err);
			return false;
		}
	}

	private boolean updateDetails(OTPModel otpModel, UserDetails userDetails) {
		otpModel.setIsVerified(true);
		userDetails.setIsContactNoVerified(true);

		otpRepo.save(otpModel);
		userRepo.save(userDetails);

		return true;
	}

	@Override
	public OTPModel resendOTP(String otpSessionId) {
		OTPModel otpModel = otpRepo.findById(otpSessionId).orElse(null);
		if (otpModel != null) {
			String otp = otpModel.getOtp();
			Date validUpto = otpModel.getValidUpto();
			String otpSMS = getMessageForOTP(otp, validUpto);
			UserDetails userDetails = otpModel.getUserDetails();
			String contactNo = userDetails.getPhoneNo();

			messageUtility.sendSMS(otpSMS, MessageType.TRANSACTIONAL, contactNo);
		}
		return otpModel;
	}
}
