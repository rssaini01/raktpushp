package com.raktpushp.service;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.raktpushp.dao.LocationsMapper;
import com.raktpushp.model.LocationsList;

@Component
public class LocationDataImpl implements LocationData {

	JdbcTemplate jdbcTemplate;
	
	private static final String SQL_NEARBY_LOCATION = "SELECT U.UserName, U.UserKey, U.BloodGroup, "
			+ "dbo.Func_Distance(L.Latitude, L.Longitude, ?, ?) AS Distance, L.Latitude, L.Longitude "
			+ "FROM Tbl_LocationUsers L INNER JOIN Tbl_UserDetails U ON U.UserUniqueId = L.UserUniqueId "
			+ "WHERE dbo.Func_Distance(L.Latitude, L.Longitude, ?, ?) <= ? AND U.BloodGroup = ?";

	@Autowired
	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<LocationsList> getNearbyLocationList(LocationsList location) {
		Object[] params = { location.getLatitude(), location.getLongitude(), location.getLatitude(),
				location.getLongitude(), location.getDistance(), location.getBloodGroup().toString() };
		
		return jdbcTemplate.query(SQL_NEARBY_LOCATION, new LocationsMapper(), params);
	}

}
