package com.raktpushp.service;

import com.raktpushp.model.OTPModel;
import com.raktpushp.model.UserDetails;

public interface OTPService {
	boolean otpVerify(OTPModel otpModel, UserDetails userDetails);

	OTPModel resendOTP(String otpSessionId);

	OTPModel sendOTP(UserDetails userDetails, String phoneNo);
}
