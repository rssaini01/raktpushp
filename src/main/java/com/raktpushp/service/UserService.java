package com.raktpushp.service;

import com.raktpushp.model.GenericResponse;
import com.raktpushp.model.UserDetails;

public interface UserService {
	GenericResponse<UserDetails> userSignUp(UserDetails userDetails);
	GenericResponse<UserDetails> userLogin(UserDetails userDetails);
	
	UserDetails getUserByKey(String userKey);
	String getLoginToken(UserDetails userDetails, String deviceId);
	GenericResponse<UserDetails> userUpdateProfile(UserDetails userDetails, String userKey);
	void userUpdateAvailableStatus(UserDetails userDetails, Boolean isAvailable);
}
