package com.raktpushp.service;

import com.raktpushp.model.GenericResponse;
import com.raktpushp.model.LocationUsers;

public interface LocationService {
	GenericResponse<LocationUsers> addUserLocation(LocationUsers locationUsers);
}
