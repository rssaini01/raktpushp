package com.raktpushp.service;

import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.raktpushp.Constants.UserRole;
import com.raktpushp.AppUtils;
import com.raktpushp.Constants.ResponseCode;
import com.raktpushp.Constants;
import com.raktpushp.Constants.Length;
import com.raktpushp.Constants.TokenStatus;
import com.raktpushp.model.GenericResponse;
import com.raktpushp.model.LoginSession;
import com.raktpushp.model.UserDetails;
import com.raktpushp.model.UserDetails_;
import com.raktpushp.repo.LoginSessionRepo;
import com.raktpushp.repo.UserDetailsRepo;
import com.raktpushp.utils.Bundle;
import com.raktpushp.utils.Bundle.Keys;

@Component
class UserServiceImpl implements UserService {

	protected UserServiceImpl() {
	}

	@Autowired
	private AppUtils appUtils;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private UserDetailsRepo userServiceRepo;

	@Autowired
	private LoginSessionRepo loginSessionRepo;

	@Override
	public GenericResponse<UserDetails> userSignUp(UserDetails userDetails) {
		GenericResponse<UserDetails> responseModel = new GenericResponse<UserDetails>();
		try {
			String emailId = userDetails.getEmail();

			Boolean passwordCheck = appUtils.passwordValidation(userDetails.getPassword(),
					userDetails.getConfirmPassword());

			if (!passwordCheck) {
				responseModel.setMessage("Password didn't match.");
				responseModel.setStatus(ResponseCode.ERROR);
				return responseModel;
			} else if (userDetails.getName() == null) {
				responseModel.setMessage("Name cannot be blank");
				responseModel.setStatus(ResponseCode.ERROR);
				return responseModel;
			} else if (emailId == null ? true : !AppUtils.validateEmail(emailId)) {
				responseModel.setMessage("Invalid E-Mail Id.");
				responseModel.setStatus(ResponseCode.ERROR);
				return responseModel;
			}

			String password = userDetails.getPassword();
			String passwordSalt = AppUtils.generateToken(Length.PASSWORD_SALT);

			String encryptedPassword = AppUtils.generateHash(password, passwordSalt);

			final String userKey = AppUtils.getUUID();
			userDetails.setUserKey(userKey);
			userDetails.setUserRole(UserRole.NORMAL_USER);
			userDetails.setIsContactNoVerified(false);
			userDetails.setIsEmailVerified(false);
			userDetails.setAccountStatus(Constants.ACTIVE_ACCOUNT);
			userDetails.setPassword(encryptedPassword);
			userDetails.setPasswordSalt(passwordSalt);
			try {
				userServiceRepo.save(userDetails);
			} catch (DataAccessException e) {
				System.err.println(e.getMessage());
				responseModel.setMessage("Duplicate Details");
				responseModel.setStatus(ResponseCode.ERROR);
				return responseModel;
			}
			userDetails.setPassword(null);
			responseModel.setStatus(ResponseCode.SUCCESS);
			responseModel.setData(userDetails);
		} catch (HibernateException e) {
			System.err.println(e.getMessage());
			responseModel.setStatus(ResponseCode.ERROR);
			responseModel.setMessage(e.getMessage());
			responseModel.setData(null);
		}
		return responseModel;
	}

	@Override
	public GenericResponse<UserDetails> userLogin(UserDetails userDetails) {
		GenericResponse<UserDetails> response = new GenericResponse<>();
		try {
			try {
				String deviceId = userDetails.getDeviceId();

				CriteriaBuilder builder = entityManager.getCriteriaBuilder();
				CriteriaQuery<UserDetails> criteria = builder.createQuery(UserDetails.class);
				Root<UserDetails> root = criteria.from(UserDetails.class);
				criteria.select(root);

				String password = userDetails.getPassword();
				Predicate loginPredicate = builder.equal(root.get(UserDetails_.phoneNo), userDetails.getPhoneNo());
				Predicate accountStatusPredicate = builder.equal(root.get(UserDetails_.accountStatus),
						Constants.ACTIVE_ACCOUNT);
				Predicate userRolePredicate = builder.equal(root.get(UserDetails_.userRole), UserRole.NORMAL_USER);

				criteria.where(builder.and(loginPredicate, accountStatusPredicate, userRolePredicate));

				try {
					userDetails = entityManager.createQuery(criteria).getSingleResult();
					String storedPassword = userDetails.getPassword();
					String encryptedPassword = AppUtils.generateHash(password, userDetails.getPasswordSalt());
					if (encryptedPassword.equals(storedPassword)) {
						String loginToken = getLoginToken(userDetails, deviceId);
						if (loginToken != null) {
							response.setStatus(ResponseCode.SUCCESS);
							response.setData(userDetails);
							response.setMessage("Successfully Logged In");
						} else {
							response.setStatus(ResponseCode.SUCCESS);
							response.setData(userDetails);
							response.setMessage("Not Logged In");
						}
					} else {
						response.setMessage("Invalid Password");
						response.setStatus(ResponseCode.ERROR);
					}
				} catch (NoResultException e) {
					System.err.println(e.getMessage());
					response.setMessage("Invalid UserName or Password");
					response.setStatus(ResponseCode.ERROR);
				}
			} catch (IndexOutOfBoundsException e) {
				System.out.println("Invalid UserName or Password");
				response.setMessage("Invalid UserName or Password");
				response.setStatus(ResponseCode.ERROR);
				userDetails.setPassword(null);
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public String getLoginToken(UserDetails userDetails, String deviceId) {
		userDetails = userServiceRepo.findByUserKey(userDetails.getUserKey()).orElse(new UserDetails());
		if (userDetails.getIsContactNoVerified()) {
			String loginToken = AppUtils.generateToken(Length.TOKEN_LENGTH);
			LoginSession loginSession = new LoginSession();
			loginSession.setUserDetails(userDetails);
			loginSession.setLoginToken(loginToken);
			loginSession.setLoginTime(Calendar.getInstance().getTime());
			loginSession.setTokenStatus(TokenStatus.LOGGED_IN);
			loginSession.setDeviceId(deviceId == null ? "Testing-Device-Id" : deviceId);
			// Save Token in Database for multiple device login or logout and control
			// session.
			loginSessionRepo.save(loginSession);
			userDetails.setToken(loginToken);

			return loginToken;
		} else
			return null;
	}

	@Override
	public UserDetails getUserByKey(String userKey) {
		UserDetails userDetails = userServiceRepo.findByUserKey(userKey).orElse(null);
		return userDetails;
	}

	@Override
	public GenericResponse<UserDetails> userUpdateProfile(UserDetails userDetails, String userKey) {
		GenericResponse<UserDetails> response = new GenericResponse<>();
		UserDetails dbUserDetails = getUserByKey(userKey);
		dbUserDetails.copyInto(userDetails);
		try {
			userServiceRepo.save(userDetails);
			response.setStatus(ResponseCode.SUCCESS);
			response.setMessage(Bundle.getString(Keys.SUCCESS));
		} catch (Exception e) {
			e.printStackTrace();
			response.setMessage(e.getMessage());
			response.setStatus(ResponseCode.ERROR);
		}
		response.setData(userDetails);
		return response;
	}

	@Override
	public void userUpdateAvailableStatus(UserDetails userDetails, Boolean isAvailable) {
		userServiceRepo.updateAvailableStatus(userDetails, isAvailable);
	}

}
