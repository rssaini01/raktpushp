package com.raktpushp.service;

import java.util.List;

import com.raktpushp.model.LocationsList;

public interface LocationData {
	List<LocationsList> getNearbyLocationList(LocationsList location);
}
