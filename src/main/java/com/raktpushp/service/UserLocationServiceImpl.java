package com.raktpushp.service;

import java.util.Calendar;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.raktpushp.Constants.LocationKeys;
import com.raktpushp.Constants.ResponseCode;
import com.raktpushp.Constants.TokenStatus;
import com.raktpushp.dao.DataAccessUtils;
import com.raktpushp.AppUtils;
import com.raktpushp.model.GenericResponse;
import com.raktpushp.model.LocationUsers;
import com.raktpushp.model.LocationUsers_;
import com.raktpushp.model.UserDetails;
import com.raktpushp.repo.LocationUsersRepo;

@Component
public class UserLocationServiceImpl implements LocationService {

	@Autowired
	private DataAccessUtils accessUtils;

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private LocationUsersRepo locationUsersRepo;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public GenericResponse<LocationUsers> addUserLocation(LocationUsers locationUsers) {
		GenericResponse<LocationUsers> response = new GenericResponse<>();
		UserDetails userDetails = accessUtils.getUserDetailsByKey(locationUsers.getUserKey());
		TokenStatus tokenStatus = accessUtils.validateUserAndToken(locationUsers.getLoginToken(), userDetails);
		if (tokenStatus == TokenStatus.LOGGED_OUT) {
			response.setMessage("User not logged in this application.");
			response.setStatus(ResponseCode.ERROR);
			return response;
		}

		updateLocationDetails(locationUsers);
		locationUsers.setUpdateDateTime(Calendar.getInstance().getTime());
		locationUsers.setUserDetails(userDetails);
		try {
		/*	TypedQuery<LocationUsers> query = session
					.createQuery("UPDATE LocationUsers SET isDefault = :isDefault WHERE userDetails = :userDetails");
			query.setParameter("isDefault", false);
			query.setParameter("userDetails", userDetails);
			query.executeUpdate();	*/

			// ------------ Update Old address from default
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaUpdate<LocationUsers> updateLocations = builder.createCriteriaUpdate(LocationUsers.class);
			Root<LocationUsers> root = updateLocations.from(LocationUsers.class);
			Expression<Boolean> expression = builder.equal(root.get(LocationUsers_.userDetails), userDetails);
			
			updateLocations.where(expression);
			updateLocations.set(LocationUsers_.isDefault, false);
			
			entityManager.createQuery(updateLocations).executeUpdate();
			
			// ------------ Save New Address of user as default
			locationUsers.setIsDefault(true);
			locationUsersRepo.save(locationUsers);
			response.setMessage("Location Successfully Added.");
			response.setStatus(ResponseCode.SUCCESS);
		} catch (HibernateException e) {
			e.printStackTrace();
			response.setMessage(e.getMessage());
			response.setStatus(ResponseCode.ERROR);
		}
		return response;
	}

	/**
	 * Update Details of Latitude & Longitude from Google Maps API
	 * 
	 * @param locationUsers
	 */
	void updateLocationDetails(LocationUsers locationUsers) {
		Map<LocationKeys, String> locationMap = AppUtils.getLocation(locationUsers.getLatitude(),
				locationUsers.getLongitude());

		locationUsers.setAddress(locationMap.get(LocationKeys.ADDRESS));
		locationUsers.setPostalCode(locationMap.get(LocationKeys.POSTAL_CODE));
		locationUsers.setPlaceId(locationMap.get(LocationKeys.PLACE_ID));
		locationUsers.setLocality(locationMap.get(LocationKeys.LOCALITY));
		locationUsers.setState(locationMap.get(LocationKeys.STATE));
	}
}
