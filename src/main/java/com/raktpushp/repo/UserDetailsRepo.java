package com.raktpushp.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.raktpushp.model.UserDetails;

@Repository
public interface UserDetailsRepo extends JpaRepository<UserDetails, Integer> {
	public Optional<UserDetails> findByUserKey(String userKey);
	
	@Query("UPDATE UserDetails SET isAvailable = ?2 WHERE id = ?1")
	public void updateAvailableStatus(UserDetails userDetails, Boolean isAvailable);
}
