package com.raktpushp.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.raktpushp.Constants.TokenStatus;
import com.raktpushp.model.LoginSession;
import com.raktpushp.model.UserDetails;

@Repository
public interface LoginSessionRepo extends JpaRepository<LoginSession, Integer> {

	@Query("from LoginSession where userDetails = ?1 and loginToken = ?2 and tokenStatus = ?3")
	Optional<LoginSession> findByKeyAndToken(UserDetails userDetails, String loginToken, TokenStatus loggedIn);

}
