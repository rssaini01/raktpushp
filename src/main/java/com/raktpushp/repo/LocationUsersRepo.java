package com.raktpushp.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.raktpushp.model.LocationUsers;

@Repository
public interface LocationUsersRepo extends JpaRepository<LocationUsers, Integer> {

}
