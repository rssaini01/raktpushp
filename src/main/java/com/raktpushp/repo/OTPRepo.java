package com.raktpushp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raktpushp.model.OTPModel;

public interface OTPRepo extends JpaRepository<OTPModel, String> {

}
