package com.raktpushp.utils;

import java.util.Locale;
import java.util.ResourceBundle;

public class Bundle {

	public enum Keys {
		INVALID_LOGIN, INVALID_IMEI, LOGIN_SUCCESS, UNAUTHORIZED_ACCESS, UNKNOWN_ERROR, INVALID_USER, SUCCESS
	}

	public static String getString(String key) {
		Locale.setDefault(new Locale("en", "IN"));
		ResourceBundle resourceBundle = ResourceBundle.getBundle("messages");
		return resourceBundle.getString(key);
	}

	public static String getString(Keys keys) {
		Locale.setDefault(new Locale("en", "IN"));
		ResourceBundle resourceBundle = ResourceBundle.getBundle("messages");
		String message;
		try {
			message = resourceBundle.getString(keys.name());
		} catch (Exception e) {
			e.printStackTrace();
			message = "No Message found for " + keys.name() + " : " + e.getClass() + " (" + e.getMessage() + ")";
		}
		return message;
	}

	public enum NotificationKeys {
		LEAVE, GENERAL
	}
}
