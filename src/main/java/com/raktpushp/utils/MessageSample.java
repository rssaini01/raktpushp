package com.raktpushp.utils;

public interface MessageSample {
	public static final String OTP_SMS_MOCK = "${otp} is your OTP to verify to your RaktPushp Account. It is valid till ${time}. Please don't share this with anyone.";
}
