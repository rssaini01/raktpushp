package com.raktpushp;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import static java.lang.Math.acos;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toRadians;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.raktpushp.model.LoginSession;
import com.raktpushp.model.UserDetails;
import com.raktpushp.repo.LoginSessionRepo;
import com.raktpushp.repo.UserDetailsRepo;

@Component
public final class AppUtils implements Constants {

	@Autowired
	private UserDetailsRepo userRepo;

	@Autowired
	private LoginSessionRepo loginRepo;

	public static Map<LocationKeys, String> getLocation(Double latitude, Double longitude) {
		Map<LocationKeys, String> locationMap = new HashMap<AppUtils.LocationKeys, String>();
		GeoApiContext gtx = new GeoApiContext().setApiKey(AppConfig.getGoogleMapApiKey());
		GeocodingResult[] gResp;
		try {
			gResp = GeocodingApi.newRequest(gtx).latlng(new LatLng(latitude, longitude)).await();
			GeocodingResult geocodingResult = gResp[0];

			String address = geocodingResult.formattedAddress;
			locationMap.put(LocationKeys.ADDRESS, address);
			String placeId = geocodingResult.placeId;
			locationMap.put(LocationKeys.PLACE_ID, placeId);
			for (AddressComponent component : geocodingResult.addressComponents) {
				if (component.types[0] == AddressComponentType.POSTAL_CODE) {
					String postalCode = component.shortName;
					locationMap.put(LocationKeys.POSTAL_CODE, postalCode);
				} else if (component.types[0] == AddressComponentType.LOCALITY) {
					String locality = component.shortName;
					locationMap.put(LocationKeys.LOCALITY, locality);
				} else if (component.types[0] == AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1) {
					String state = component.longName;
					locationMap.put(LocationKeys.STATE, state);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return locationMap;
	}

	public boolean passwordValidation(String password1, String password2) {
		if (password1 != null)
			return password1.equals(password2);
		else
			return false;
	}

	public String generateUUID() {
		return UUID.randomUUID().toString().toUpperCase(LOCALE);
	}

	public static boolean validateEmail(String emailId) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailId);
		return matcher.find();
	}

	public static String generateToken(Length length) {
		StringBuilder builder = new StringBuilder();
		int count = length.getValue();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	public static String getOTP(int len) {
		Random random = new Random();
		char[] otp = new char[len];
		for (int i = 0; i < len; i++) {
			otp[i] = NUMBERS.charAt(random.nextInt(NUMBERS.length()));
		}
		return new String(otp);
	}

	public static String getUUID() {
		return UUID.randomUUID().toString().toUpperCase();
	}

	public static String generateHash(String password, String salt) {
		StringBuilder hash = new StringBuilder();
		String input = salt + password;
		try {
			MessageDigest sha = MessageDigest.getInstance("SHA-256");
			byte[] hashedBytes = sha.digest(input.getBytes());
			char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
			for (int idx = 0; idx < hashedBytes.length; ++idx) {
				byte b = hashedBytes[idx];
				hash.append(digits[(b & 0xf0) >> 4]);
				hash.append(digits[b & 0x0f]);
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hash.toString();
	}

	public LoginSession validateKeyAndToken(String userKey, String loginToken) {
		UserDetails userModel = userRepo.findByUserKey(userKey).orElse(null);
		LoginSession loginSession = loginRepo.findByKeyAndToken(userModel, loginToken, TokenStatus.LOGGED_IN)
				.orElse(null);
		if (loginSession != null) {
			loginSession.setLastAccessedTime(new Date());
			loginRepo.save(loginSession);
		}
		return loginSession;
	}

	public static Double getDistanceFromLocations(Double lat1, Double lng1, Double lat2, Double lng2) {
		final int earthRadius = 6371;
		Double distance = earthRadius
				* acos(cos(toRadians(lat1)) * cos(toRadians(lat2)) * cos(toRadians(lng2) - toRadians(lng1))
						+ sin(toRadians(lat1)) * sin(toRadians(lat2)));
		return distance;
	}
}
