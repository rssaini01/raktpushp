package com.raktpushp.model;

public class PropertiesModel implements AppModel {
	private static final long serialVersionUID = -1992647999829951301L;
	private static PropertiesModel propertiesModel = new PropertiesModel();
	
	public static PropertiesModel getPropertiesModel() {
		return propertiesModel;
	}
	
	public static void setPropertiesModel(PropertiesModel propertiesModel) {
		PropertiesModel.propertiesModel = propertiesModel;
	}
	
}
