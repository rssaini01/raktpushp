package com.raktpushp.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.raktpushp.Constants.TokenStatus;

@Entity
@Table(name = "Tbl_LoginSession")
public class LoginSession implements AppModel {
	private static final long serialVersionUID = -4488182982406887413L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "LoginSessionId")
	private Integer loginSessionId;

	@Column(name = "LoginTime")
	private Date loginTime;

	@Column(name = "LogoutTime")
	private Date logoutTime;

	@Column(name = "DeviceId")
	private String deviceId;

	@Column(name = "LoginToken")
	private String loginToken;

	@ManyToOne(cascade = CascadeType.ALL)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "UserUniqueId")
	private UserDetails userDetails;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "TokenStatus")
	private TokenStatus tokenStatus;

	@Column(name = "LastAccessedTime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastAccessedTime;

	public Integer getLoginSessionId() {
		return loginSessionId;
	}

	public void setLoginSessionId(Integer loginSessionId) {
		this.loginSessionId = loginSessionId;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Date getLogoutTime() {
		return logoutTime;
	}

	public void setLogoutTime(Date logoutTime) {
		this.logoutTime = logoutTime;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getLoginToken() {
		return loginToken;
	}

	public void setLoginToken(String loginToken) {
		this.loginToken = loginToken;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public TokenStatus getTokenStatus() {
		return tokenStatus;
	}

	public void setTokenStatus(TokenStatus tokenStatus) {
		this.tokenStatus = tokenStatus;
	}

	public Date getLastAccessedTime() {
		return lastAccessedTime;
	}

	public void setLastAccessedTime(Date lastAccessedTime) {
		this.lastAccessedTime = lastAccessedTime;
	}
}