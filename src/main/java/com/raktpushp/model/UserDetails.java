package com.raktpushp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.raktpushp.Constants.BloodGroup;
import com.raktpushp.Constants.Gender;
import com.raktpushp.Constants.UserRole;

@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlRootElement
@Entity
@Table(name = "Tbl_UserDetails")
public class UserDetails implements AppModel {
	private static final long serialVersionUID = 6536180645450726282L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "UserUniqueId", updatable = false, nullable = false)
	private Integer userId;

	@Column(name = "UserName")
	private String name;

	@Column(name = "EmailId")
	private String email;

	@Column(name = "ContactNo")
	private String phoneNo;

	@Column(name = "UserKey")
	private String userKey;

	@Column(name = "BloodGroup")
	@Enumerated(EnumType.STRING)
	private BloodGroup bloodGroup;

	@Column(name = "Password")
	private String password;

	@Column(name = "AccountStatus")
	private Integer accountStatus;

	@Column(name = "UserRole")
	@Enumerated(EnumType.STRING)
	private UserRole userRole;

	@Column(name = "ContactNoVerified")
	private Boolean isContactNoVerified;

	@Column(name = "EmailVerified")
	private Boolean isEmailVerified;

	@Column(name = "Available")
	private Boolean isAvailaable = false;

	@Column(name = "PasswordSalt")
	@JsonIgnore
	private String passwordSalt;

	@Column(name = "Gender")
	@Enumerated(EnumType.STRING)
	private Gender gender;

	@Transient
	private Boolean isAddressAdded;

	@Transient
	private String confirmPassword, otp, role, deviceId, token, photoLink;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPhotoLink() {
		return photoLink;
	}

	public void setPhotoLink(String photoLink) {
		this.photoLink = photoLink;
	}

	@JsonIgnore(true)
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	@JsonIgnore(true)
	public String getConfirmPassword() {
		return confirmPassword;
	}

	@JsonProperty
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Integer getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(Integer accountStatus) {
		this.accountStatus = accountStatus;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	public Boolean getIsContactNoVerified() {
		return isContactNoVerified;
	}

	public void setIsContactNoVerified(Boolean isContactNoVerified) {
		this.isContactNoVerified = isContactNoVerified;
	}

	public Boolean getIsEmailVerified() {
		return isEmailVerified;
	}

	public void setIsEmailVerified(Boolean isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public BloodGroup getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(BloodGroup bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public Boolean getIsAddressAdded() {
		return isAddressAdded;
	}

	public void setIsAddressAdded(Boolean isAddressAdded) {
		this.isAddressAdded = isAddressAdded;
	}

	public String getPasswordSalt() {
		return passwordSalt;
	}

	public void setPasswordSalt(String passwordSalt) {
		this.passwordSalt = passwordSalt;
	}

	public Boolean getIsAvailaable() {
		return isAvailaable;
	}

	public void setIsAvailaable(Boolean isAvailaable) {
		this.isAvailaable = isAvailaable;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "UserDetails [name=" + name + ", email=" + email + ", phoneNo=" + phoneNo + ", userKey=" + userKey
				+ ", token=" + token + ", password=" + password + ", accountStatus=" + accountStatus + ", userId="
				+ userId + ", deviceid=" + deviceId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		result = prime * result + ((userKey == null) ? 0 : userKey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserDetails other = (UserDetails) obj;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		if (userKey == null) {
			if (other.userKey != null)
				return false;
		} else if (!userKey.equals(other.userKey))
			return false;
		return true;
	}

	public void copyInto(UserDetails obj) {
		obj.userId = obj.userId == null ? this.userId : obj.userId;
		obj.name = obj.name == null ? this.name : obj.name;
		obj.email = obj.email == null ? this.email : obj.email;
		obj.phoneNo = obj.phoneNo == null ? this.phoneNo : obj.phoneNo;
		obj.userKey = obj.userKey == null ? this.userKey : obj.userKey;
		obj.bloodGroup = obj.bloodGroup == null ? this.bloodGroup : obj.bloodGroup;
		obj.password = obj.password == null ? this.password : obj.password;
		obj.accountStatus = obj.accountStatus == null ? this.accountStatus : obj.accountStatus;
		obj.userRole = obj.userRole == null ? this.userRole : obj.userRole;
		obj.isContactNoVerified = obj.isContactNoVerified == null ? this.isContactNoVerified : obj.isContactNoVerified;
		obj.isEmailVerified = obj.isEmailVerified == null ? this.isEmailVerified : obj.isEmailVerified;
		obj.passwordSalt = obj.passwordSalt == null ? this.passwordSalt : obj.passwordSalt;
		obj.gender = obj.gender == null ? this.gender : obj.gender;
	}

}
