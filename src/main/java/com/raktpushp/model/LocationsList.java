package com.raktpushp.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.raktpushp.Constants.BloodGroup;

@JsonInclude(Include.NON_NULL)
public class LocationsList implements Comparable<LocationsList>, AppModel {
	private static final long serialVersionUID = 1090105924534308820L;
	
	private String userName, userKey, phoneNo;
	private BloodGroup bloodGroup;
	private Integer distance;
	private Double latitude, longitude;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public BloodGroup getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(BloodGroup bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public Integer getDistance() {
		return distance;
	}

	public void setDistance(Integer distance) {
		this.distance = distance;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "LocationsList [userName=" + userName + ", userKey=" + userKey + ", phoneNo=" + phoneNo + ", bloodGroup="
				+ bloodGroup + ", distance=" + distance + ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}

	@Override
	public int hashCode() {
		String str = userKey + latitude + longitude + distance;
		return str.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		final LocationsList other = (LocationsList) obj;

		if (other.userKey == null)
			return false;
		if (other.distance == null)
			return false;
		if (other.latitude == null)
			return false;
		if (other.longitude == null)
			return false;

		if (other.userKey.equals(this.userKey) && other.distance.equals(this.distance)
				&& other.latitude.equals(this.latitude) && other.longitude.equals(this.longitude))
			return true;
		return false;
	}

	@Override
	public int compareTo(LocationsList loc) {
		return this.distance - loc.distance;
	}
}
