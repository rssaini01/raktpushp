package com.raktpushp.model;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.raktpushp.Constants.ResponseCode;
import com.raktpushp.Constants.TokenStatus;

@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenericResponse<T> {
	private Integer status;
	private String message;
	private T data;
	private TokenStatus tokenStatus;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(ResponseCode status) {
		this.status = status.getValue();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public TokenStatus getTokenStatus() {
		return tokenStatus;
	}

	public void setTokenStatus(TokenStatus tokenStatus) {
		this.tokenStatus = tokenStatus;
	}

	@Override
	public String toString() {
		return "GenericResponse [status=" + status + ", message=" + message + ", data=" + data + "]";
	}
}
