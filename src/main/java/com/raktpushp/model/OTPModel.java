package com.raktpushp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "Tbl_OTPLog")
@JsonInclude(JsonInclude.Include.NON_NULL)
@DynamicInsert(true)
@DynamicUpdate(true)
public class OTPModel {
	@Column(name = "OTP")
	private String otp;

	@ManyToOne
	@JoinColumn(name = "UserId")
	private UserDetails userDetails;

	@Column(name = "ValidUpto")
	@Temporal(TemporalType.TIMESTAMP)
	private Date validUpto;

	@Column(name = "Verified", nullable = false, columnDefinition = "bit default 0")
	private Boolean isVerified;

	@Id
	@Column(name = "OtpSessionId")
	private String otpSessionId;

	@Transient
	private String deviceId;

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@JsonIgnore(true)
	public String getOtp() {
		return otp;
	}

	@JsonProperty
	public void setOtp(String otp) {
		this.otp = otp;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public Date getValidUpto() {
		return validUpto;
	}

	public void setValidUpto(Date validUpto) {
		this.validUpto = validUpto;
	}

	public Boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public String getOtpSessionId() {
		return otpSessionId;
	}

	public void setOtpSessionId(String otpSessionId) {
		this.otpSessionId = otpSessionId;
	}

}
