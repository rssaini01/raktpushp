package com.raktpushp;

final public class AppConfig {
	private static String googleMapApiKey = "AIzaSyCFo1lopaL460HtyJdP7pEwWQUOeTmz-x4";
	private static Double distance = 20.0;
	private static Integer otpLength = 6;

	public static String getGoogleMapApiKey() {
		return googleMapApiKey;
	}

	public void setGoogleMapApiKey(String googleMapApiKey) {
		AppConfig.googleMapApiKey = googleMapApiKey;
	}

	public static Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		AppConfig.distance = distance;
	}
	
	public static Integer getOtpLength() {
		return otpLength;
	}
}
