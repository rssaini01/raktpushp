package com.raktpushp.controller;

import java.util.TreeSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

import com.raktpushp.AppConfig;
import com.raktpushp.Constants.ResponseCode;
import com.raktpushp.model.GenericResponse;
import com.raktpushp.model.LocationUsers;
import com.raktpushp.model.LocationsList;
import com.raktpushp.service.LocationData;
import com.raktpushp.service.LocationService;

@Controller
@RequestMapping("location")
@Scope(value = WebApplicationContext.SCOPE_APPLICATION)
public class LocationController {
	@Autowired
	LocationData locationData;

	@Autowired
	private LocationService locationService;

	@RequestMapping(value = "nearby", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
	public @ResponseBody GenericResponse<Set<LocationsList>> getAllNearbyLocations(@RequestBody LocationsList location,
			@RequestHeader("UserKey") String userKey, @RequestHeader("LoginToken") String loginToken) {
		GenericResponse<Set<LocationsList>> genericResponse = new GenericResponse<>();
		if (location.getDistance() > AppConfig.getDistance()) {
			genericResponse.setStatus(ResponseCode.ERROR);
			genericResponse.setMessage("Distance can't be greater than " + AppConfig.getDistance());
			return genericResponse;
		}
		// location.setDistance(AppConfig.getDistance());
		location.setDistance(20); // Hard coded from bitbucket cloud
		List<LocationsList> locationList = locationData.getNearbyLocationList(location);
		genericResponse.setMessage("Success");
		genericResponse.setStatus(ResponseCode.SUCCESS);
		Set<LocationsList> locationSet = new TreeSet<>(locationList);
		genericResponse.setData(locationSet);
		return genericResponse;
	}

	@RequestMapping(value = "add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
	public @ResponseBody GenericResponse<LocationUsers> addUsersLocation(@RequestBody LocationUsers locationUsers,
			@RequestHeader("UserKey") String userKey, @RequestHeader("LoginToken") String loginToken) {
		GenericResponse<LocationUsers> responseModel = locationService.addUserLocation(locationUsers);
		System.out.println(responseModel);
		return responseModel;
	}
}
