package com.raktpushp.controller;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.raktpushp.Constants.ResponseCode;
import com.raktpushp.model.GenericResponse;
import com.raktpushp.model.OTPModel;
import com.raktpushp.model.UserDetails;
import com.raktpushp.service.OTPService;
import com.raktpushp.service.UserService;

@Controller
@RequestMapping(value = "otp")
public class OTPController {

	@Autowired
	private OTPService otpService;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "send/{phoneNo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public @ResponseBody GenericResponse<OTPModel> sendOTP(@RequestHeader("userKey") String userKey, @PathVariable("phoneNo") String phoneNo) {
		UserDetails userDetails = userService.getUserByKey(userKey);
		GenericResponse<OTPModel> response = new GenericResponse<OTPModel>();
		if (userDetails != null) {
			OTPModel otpModel = otpService.sendOTP(userDetails, phoneNo);
			response.setData(otpModel);
			response.setStatus(ResponseCode.SUCCESS);
		} else {
			response.setStatus(ResponseCode.ERROR);
			response.setMessage("Invalid User");
		}
		return response;
	}

	@RequestMapping(value = "resend", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public @ResponseBody GenericResponse<OTPModel> resendOTP(@RequestHeader("otpSessionId") String otpSessionId) {
		GenericResponse<OTPModel> response = new GenericResponse<OTPModel>();
		if (otpSessionId != null) {
			OTPModel otpModel = otpService.resendOTP(otpSessionId);
			response.setData(otpModel);
			response.setStatus(ResponseCode.SUCCESS);
		} else {
			response.setStatus(ResponseCode.ERROR);
			response.setMessage("Invalid User");
		}
		return response;
	}

	@RequestMapping(value = "verify", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
	public @ResponseBody GenericResponse<UserDetails> verifyOTP(@RequestHeader("UserKey") String userKey,
			@RequestBody OTPModel otpModel) {
		UserDetails userDetails = userService.getUserByKey(userKey);
		String deviceId = otpModel.getDeviceId();
		GenericResponse<UserDetails> response = new GenericResponse<UserDetails>();

		if (userDetails != null) {
			boolean flag = otpService.otpVerify(otpModel, userDetails);
			if (flag) {
				String loginToken = userService.getLoginToken(userDetails, deviceId);
				userDetails.setToken(loginToken);
				response.setData(userDetails);
				response.setStatus(ResponseCode.SUCCESS);
				response.setMessage("Success");
			} else {
				response.setStatus(ResponseCode.ERROR);
				response.setMessage("Invalid or Expired OTP");
			}
		} else {
			response.setStatus(ResponseCode.ERROR);
			response.setMessage("Mismatched User Details");
		}

		return response;
	}
}
