package com.raktpushp.controller;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

import com.raktpushp.AppUtils;
import com.raktpushp.Constants.ResponseCode;
import com.raktpushp.Constants.TokenStatus;
import com.raktpushp.model.GenericResponse;
import com.raktpushp.model.LoginSession;
import com.raktpushp.model.UserDetails;
import com.raktpushp.service.UserService;
import com.raktpushp.utils.Bundle;
import com.raktpushp.utils.Bundle.Keys;

@Controller
@RequestMapping("user")
@Scope(value = WebApplicationContext.SCOPE_APPLICATION)
public class UserController {
	@Autowired
	private UserService userService;

	@Autowired
	private AppUtils appUtils;
	
	@RequestMapping(value = "sign-up", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
	public @ResponseBody GenericResponse<UserDetails> userSignUp(@RequestBody UserDetails userDetails) {
		GenericResponse<UserDetails> response = userService.userSignUp(userDetails);
		return response;
	}

	@RequestMapping(value = "login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
	public @ResponseBody GenericResponse<UserDetails> userLogin(@RequestBody UserDetails userDetails) {
		GenericResponse<UserDetails> response = userService.userLogin(userDetails);
		return response;
	}

	@RequestMapping(value = "profile", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON)
	public @ResponseBody GenericResponse<UserDetails> userUpdateProfile(@RequestHeader("loginToken") String loginToken,
			@RequestHeader("userKey") String userKey, @RequestBody UserDetails userDetails) {
		LoginSession loginSession = appUtils.validateKeyAndToken(userKey, loginToken);

		GenericResponse<UserDetails> response = new GenericResponse<>();
		if (loginSession == null) {
			response.setStatus(ResponseCode.ERROR);
			response.setTokenStatus(TokenStatus.LOGGED_OUT);
			response.setMessage(Bundle.getString(Keys.UNAUTHORIZED_ACCESS));
			return response;
		}
		response = userService.userUpdateProfile(userDetails, userKey);
		return response;
	}
	
	@RequestMapping(value = "profile", method = RequestMethod.GET)
	public @ResponseBody GenericResponse<UserDetails> getUserProfile(@RequestHeader("loginToken") String loginToken,
			@RequestHeader("userKey") String userKey) {
		LoginSession loginSession = appUtils.validateKeyAndToken(userKey, loginToken);

		GenericResponse<UserDetails> response = new GenericResponse<>();
		if (loginSession == null) {
			response.setStatus(ResponseCode.ERROR);
			response.setTokenStatus(TokenStatus.LOGGED_OUT);
			response.setMessage(Bundle.getString(Keys.UNAUTHORIZED_ACCESS));
			return response;
		}
		response.setData(loginSession.getUserDetails());
		response.setMessage(Bundle.getString(Keys.SUCCESS));
		
		return response;
	}
	
	@RequestMapping(value = "available", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse<UserDetails> isAvailableSwitch(@RequestHeader("loginToken") String loginToken,
			@RequestHeader("userKey") String userKey, @RequestParam("status") Boolean isAvailable) {
		LoginSession loginSession = appUtils.validateKeyAndToken(userKey, loginToken);

		GenericResponse<UserDetails> response = new GenericResponse<>();
		if (loginSession == null) {
			response.setStatus(ResponseCode.ERROR);
			response.setTokenStatus(TokenStatus.LOGGED_OUT);
			response.setMessage(Bundle.getString(Keys.UNAUTHORIZED_ACCESS));
			return response;
		}
		userService.userUpdateAvailableStatus(loginSession.getUserDetails(), isAvailable);
		response.setData(loginSession.getUserDetails());
		response.setMessage(Bundle.getString(Keys.SUCCESS));
		
		return response;
	}
	
}
