package com.raktpushp;

import org.hibernate.Hibernate;
//import org.hibernate.SessionFactory;
//import org.hibernate.cfg.Configuration;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;

@Scope(value = WebApplicationContext.SCOPE_APPLICATION)
public final class HibernateUtils {

//	static final SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

//	public static SessionFactory getHibernateSession() {
//		return sessionFactory;
//	}

	@SuppressWarnings("unchecked")
	public static <T> T initializeAndUnproxy(T entity) {
		if (entity == null) {
			throw new NullPointerException("Entity passed for initialization is null");
		}

		Hibernate.initialize(entity);
		if (entity instanceof HibernateProxy) {
			entity = (T) ((HibernateProxy) entity).getHibernateLazyInitializer().getImplementation();
		}
		return entity;
	}

	private HibernateUtils() {
	}
}
