package com.raktpushp.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import com.raktpushp.Constants.TokenStatus;
import com.raktpushp.Constants;
import com.raktpushp.model.LoginSession;
import com.raktpushp.model.LoginSession_;
import com.raktpushp.model.UserDetails;
import com.raktpushp.model.UserDetails_;

@Component
public class DataAccessUtils {

	@PersistenceContext
	private EntityManager entityManager;

	public TokenStatus validateUserAndToken(String loginToken, UserDetails userDetails) {
		try {
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<LoginSession> criteria = builder.createQuery(LoginSession.class);
			Root<LoginSession> root = criteria.from(LoginSession.class);
			criteria.select(root);

			Predicate loginPredicate = builder.equal(root.get(LoginSession_.loginToken), loginToken);
			Predicate detailsPredicate = builder.equal(root.get(LoginSession_.userDetails), userDetails);

			criteria.where(builder.and(loginPredicate, detailsPredicate));

			LoginSession loginSession = null;
			try {
				loginSession = entityManager.createQuery(criteria).getSingleResult();
			} catch (NoResultException e) {
				// Ignore this because as per my logic this is OK!
				System.err.println(e.getMessage());
			}
			if (loginSession == null)
				return TokenStatus.LOGGED_OUT;

			return loginSession.getTokenStatus();
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			return TokenStatus.LOGGED_OUT;
		} catch (HibernateException e) {
			e.printStackTrace();
			return TokenStatus.LOGGED_OUT;
		}
	}

	public UserDetails getUserDetailsByKey(String userKey) {
		try {
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<UserDetails> criteria = builder.createQuery(UserDetails.class);
			Root<UserDetails> root = criteria.from(UserDetails.class);
			criteria.select(root);

			Predicate keyPredicate = builder.equal(root.get(UserDetails_.userKey), userKey);
			Predicate statusPredicate = builder.equal(root.get(UserDetails_.accountStatus), Constants.ACTIVE_ACCOUNT);

			criteria.where(builder.and(keyPredicate, statusPredicate));

			UserDetails userDetails = null;
			try {
				userDetails = entityManager.createQuery(criteria).getSingleResult();
			} catch (NoResultException e) {
				// Ignore this because as per my logic this is OK!
				System.err.println(e.getMessage());
			}
			return userDetails;
		} catch (IndexOutOfBoundsException e) {
			System.err.println(e.getMessage());
			return null;
		} catch (HibernateException e) {
			e.printStackTrace();
			return null;
		}
	}
}
