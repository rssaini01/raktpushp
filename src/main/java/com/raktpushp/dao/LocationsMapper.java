package com.raktpushp.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.raktpushp.Constants.BloodGroup;
import com.raktpushp.model.LocationsList;

public class LocationsMapper implements RowMapper<LocationsList> {

	@Override
	public LocationsList mapRow(ResultSet resultSet, int i) throws SQLException {
		LocationsList locationsList = new LocationsList();
		locationsList.setBloodGroup(BloodGroup.valueOf(resultSet.getString("BloodGroup")));
		locationsList.setDistance(resultSet.getInt("Distance"));
		locationsList.setUserKey(resultSet.getString("UserKey"));
		locationsList.setLatitude(resultSet.getDouble("Latitude"));
		locationsList.setLongitude(resultSet.getDouble("Longitude"));
		locationsList.setUserName(resultSet.getString("UserName"));

		return locationsList;
	}

}
