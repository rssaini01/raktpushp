package test.raktpushp;

import static java.lang.Math.*;

public class TestLocation {
	static double lat1 = 28.5733277; // current
	static double lng1 = 77.3820875;

	static double lat2 = 28.5717881; // from db
	static double lng2 = 77.3250148;

	static int earthRadiusInKiloMeter = 6371;
	static int earthRadiusInMiles = 3959;

	public static void main(String[] args) {

		Double distance = earthRadiusInKiloMeter
				* acos(cos(toRadians(lat1)) * cos(toRadians(lat2)) * cos(toRadians(lng2) - toRadians(lng1))
						+ sin(toRadians(lat1)) * sin(toRadians(lat2)));

		System.out.println(distance);

		System.out.println(round(distance));
	}
}

/*
 * 28.5733277,77.3820875,16z
 * 
 * 6371 * acos( cos(radians(@lat)) * cos(radians(lat1)) * cos(radians(long1) -
 * radians(@lng)) + sin(radians(@lat)) * sin(radians(lat1)) )
 */